<?php

namespace Drupal\change_requests\Access;


use Drupal\change_requests\Entity\Patch;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Determines access to node previews.
 *
 * @ingroup node_access
 */
class PatchApplyAccessCheck implements AccessInterface {

  /**
   * Checks access to the node preview page.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   * @param \Drupal\change_requests\Entity\Patch $patch
   *   The patch that is going to be applied.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account, Patch $patch) {
    return $patch->access('apply', $account, TRUE);
  }

}
