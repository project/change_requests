<?php

namespace Drupal\change_requests\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\node\Entity\Node;
use Drupal\change_requests\Entity\Patch;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\change_requests\DiffService;
use Drupal\change_requests\Plugin\FieldPatchPluginManager;

/**
 * Class PatchApplyController.
 */
class PatchApplyController extends ControllerBase {

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Drupal\change_requests\DiffService definition.
   *
   * @var \Drupal\change_requests\DiffService
   */
  protected $changeRequestsDiff;

  /**
   * Drupal\change_requests\Plugin\FieldPatchPluginManager definition.
   *
   * @var \Drupal\change_requests\Plugin\FieldPatchPluginManager
   */
  protected $pluginManagerFieldPatchPlugin;

  /**
   * Drupal\change_requests\Entity\Patch definition.
   *
   * @var \Drupal\change_requests\Entity\Patch|false
   */
  protected $patch;

  /**
   * Drupal\node\NodeInterface definition.
   *
   * @var \Drupal\node\NodeInterface|false
   */
  protected $node;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new PatchApplyController object.
   */
  public function __construct(
    EntityTypeManager $entity_type_manager,
    DiffService $change_requests_diff,
    FieldPatchPluginManager $plugin_manager_field_patch_plugin,
    MessengerInterface $messenger
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->changeRequestsDiff = $change_requests_diff;
    $this->pluginManagerFieldPatchPlugin = $plugin_manager_field_patch_plugin;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('change_requests.diff'),
      $container->get('plugin.manager.field_patch_plugin'),
      $container->get('messenger')
    );
  }

  /**
   * Apply patch.
   *
   * @param int $patch
   *   The patch ID.
   *
   * @return array
   *   Rendered .
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function apply($patch) {
    // Set patch or die.
    $this->patch = $this->entityTypeManager->getStorage('patch')->load($patch);
    if (!$this->patch) {
      $this->messenger->addWarning($this->t('Patch with ID: @id could not be found.', ['@id' => $patch]));
      return [];
    }
    $this->node = $this->patch->originalEntity();
    if (!$this->node) {
      $this->messenger->addWarning($this->t('The original node for this patch does not exist anymore.'));
      return [];
    }

    return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: apply with parameter(s): $patch'),
    ];
  }

}
