<?php

namespace Drupal\change_requests\Entity;

use Drupal\change_requests\Events\ChangeRequests;
use Drupal\Core\Entity\EntityMalformedException;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines the Patch entity.
 *
 * @ingroup change_requests
 *
 * @ContentEntityType(
 *   id = "patch",
 *   label = @Translation("Change request"),
 *   label_collection = @Translation("Change requests"),
 *   label_singular = @Translation("change request"),
 *   label_plural = @Translation("change requests"),
 *   label_count = @PluralTranslation(
 *     singular = "@count change request",
 *     plural = "@count change requests"
 *   ),
 *   handlers = {
 *     "view_builder" = "Drupal\change_requests\PatchViewBuilder",
 *     "access" = "Drupal\change_requests\PatchAccessControlHandler",
 *     "views_data" = "Drupal\change_requests\Entity\PatchViewsData",
 *     "form" = {
 *       "default" = "Drupal\change_requests\Form\PatchForm",
 *       "delete" = "Drupal\change_requests\Form\PatchDeleteForm",
 *       "edit" = "Drupal\change_requests\Form\PatchForm",
 *       "apply" = "Drupal\change_requests\Form\PatchApplyForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\change_requests\PatchHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "patch",
 *   admin_permission = "administer patch entities",
 *   list_cache_contexts = {
 *     "user.permissions",
 *     "languages",
 *     "timezone",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "created" = "created",
 *     "changed" = "changed",
 *     "uuid" = "uuid",
 *     "status" = "status",
 *     "rtype" = "rtype",
 *     "rbundle" = "rbundle",
 *     "rid" = "rid",
 *     "rvid" = "rvid",
 *     "uid" = "uid",
 *     "patch" = "patch",
 *     "message" = "message",
 *   },
 *   links = {
 *     "canonical" = "/patch/{patch}",
 *     "apply-form" = "/patch/{patch}/apply",
 *     "edit-form" = "/patch/{patch}/edit",
 *     "delete-form" = "/patch/{patch}/delete",
 *   }
 * )
 */
class Patch extends ContentEntityBase {

  /**
   * The original entity the patch is abstracted from.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $originalEntity;

  /**
   * The user who created the patch.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $creator;

  /**
   * The Drupal entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * The entity field map.
   *
   * @var array
   */
  protected $entityFieldMap;

  /**
   * The change_requests.diff service as adapter to diff_match_patch.
   *
   * @var \Drupal\change_requests\DiffService
   */
  public $diffService;

  /**
   * The change_requests field patch manager.
   *
   * @var \Drupal\change_requests\Plugin\FieldPatchPluginManager
   */
  private $pluginManager;

  /**
   * The revision_ids from the original entity.
   *
   * @var array
   */
  private $origRevisionIds;

  /**
   * The complete collection of all diffs stored in the entity.
   *
   * @var array
   */
  private $diff;

  /**
   * Class managing the status of change requests.
   *
   * @var \Drupal\change_requests\Events\ChangeRequests
   */
  public $statusConstants;

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->get('message')->getString();
  }

  /**
   * Get status of change request, if recommended, applied or declined.
   *
   * @param bool $literal
   *   Return a literal translatable value or if false just the string id.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup|string
   *   Returns the current status of the entity.
   */
  public function getStatus($literal = FALSE) {
    if (!$this->statusConstants) {
      $this->statusConstants = new ChangeRequests();
    }
    $status_id = (int) $this->get('status')->getString();
    return $literal
      ? $this->statusConstants->getStatusLiteral($status_id)
      : $this->statusConstants->getStatus($status_id);
  }

  /**
   * Returns formatted.
   *
   * @return array
   *   The formatted string for List overview.
   */
  public function getRenderedStatus() {
    return [
      '#theme' => 'cr_status',
      '#status' => $this->getStatus(true),
      '#attributes' => ['class' => [ $this->getStatus() ]],
    ];
  }

  /**
   * Returns lazy instance of field patch plugin manager.
   *
   * @return \Drupal\change_requests\Plugin\FieldPatchPluginManager
   *   The field patch plugin manager.
   */
  public function getPluginManager() {
    if (!$this->pluginManager) {
      $this->pluginManager = \Drupal::service('plugin.manager.field_patch_plugin');
    }
    return $this->pluginManager;
  }

  /**
   * Returns the Diff entity.
   *
   * @return \Drupal\change_requests\DiffService
   *   Returns lazy instance of the change_requests.diff service.
   */
  public function getDiffService() {
    if (!$this->diffService) {
      $this->diffService = \Drupal::service('change_requests.diff');
    }
    return $this->diffService;
  }

  /**
   * Returns lazy Entity field manager.
   *
   * @return \Drupal\Core\Entity\EntityFieldManager
   *   Returns lazy instance of entity field manager.
   */
  protected function getEntityFieldManager() {
    if (!$this->entityFieldManager) {
      $this->entityFieldManager = \Drupal::service('entity_field.manager');
    }
    return $this->entityFieldManager;
  }

  /**
   * Returns lazy Entity field map.
   *
   * @return array
   *   The complete field map.
   */
  protected function getEntityFieldMap() {
    if (!$this->entityFieldMap) {
      $this->entityFieldMap = $this->getEntityFieldManager()->getFieldMap();
    }
    return $this->entityFieldMap;
  }

  /**
   * Field type property.
   *
   * @param string $field_name
   *   The field machine name.
   *
   * @return string
   *   Returns field_type from field map of entity type.
   */
  public function getEntityFieldType($field_name) {
    $entity_id = $this->entityKeys['rtype'];
    $map = $this->getEntityFieldMap();
    return (isset($map[$entity_id][$field_name]))
      ? $map[$entity_id][$field_name]['type']
      : '';
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields[$entity_type->getKey('id')] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('ID'))
      ->setReadOnly(TRUE)
      ->setSetting('unsigned', TRUE);

    $fields[$entity_type->getKey('created')] = BaseFieldDefinition::create('created')
      ->setLabel(t('Added on'))
      ->setDescription(t('The time that the patch was created.'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'timestamp',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', FALSE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the node was last edited.'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    $fields[$entity_type->getKey('uuid')] = BaseFieldDefinition::create('uuid')
      ->setLabel(new TranslatableMarkup('UUID'))
      ->setReadOnly(TRUE);

    $fields[$entity_type->getKey('status')] = BaseFieldDefinition::create('integer')
      ->setLabel(t('status'))
      ->setDescription(t('The status of the patch.'))
      ->setSetting('size', 'small');

    $fields[$entity_type->getKey('rtype')] = BaseFieldDefinition::create('string')
      ->setLabel(t('type of referred node.'))
      ->setRevisionable(FALSE)
      ->setDefaultValue('');

    $fields[$entity_type->getKey('rbundle')] = BaseFieldDefinition::create('string')
      ->setLabel(t('bundle of referred node'))
      ->setRevisionable(FALSE)
      ->setDefaultValue('');

    $fields[$entity_type->getKey('rid')] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Referred node'))
      ->setDescription(t('The referred node of the patch.'))
      ->setReadOnly(TRUE)
      ->setSetting('target_type', 'node');

    $fields[$entity_type->getKey('rvid')] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Node version id'))
      ->setReadOnly(TRUE)
      ->setDefaultValue('')
      ->setSetting('unsigned', TRUE);

    $fields[$entity_type->getKey('uid')] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Creator'))
      ->setDescription(t('The creator of the patch.'))
      ->setReadOnly(TRUE)
      ->setSetting('target_type', 'user');

    $fields[$entity_type->getKey('patch')] = BaseFieldDefinition::create('map')
      ->setLabel(new TranslatableMarkup('Patch'))
      ->setReadOnly(TRUE)
      ->setRevisionable(FALSE)
      ->setDefaultValue('');

    $fields[$entity_type->getKey('message')] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Revision log message'))
      ->setRevisionable(TRUE)
      ->setDefaultValue('');

    return $fields;
  }

  /**
   * Returns patch for all changed fields.
   *
   * @return array
   *   Returns conten of patch field.
   */
  public function getPatchField() {
    if (!$this->diff) {
      $patch = $this->get('patch')->getValue();
      $this->diff = (count($patch)) ? $patch[0] : [];
    }
    return $this->diff;
  }

  /**
   * Returns patch for a single field.
   *
   * @param string $field_name
   *   The field name to return.
   *
   * @return array
   *   The field value.
   */
  public function getPatchValue($field_name = '') {
    $patch = $this->getPatchField();
    if (isset($patch[$field_name])) {
      return $patch[$field_name];
    }
    else {
      return [];
    }
  }

  /**
   * Returns all revision_ids for an entity.
   *
   * @return array
   *   Returns complete array of revision ids.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getOrigRevisionIds() {
    if (!$this->origRevisionIds) {
      $entity = $this->originalEntity();
      $entity_type = $entity->getEntityType();
      if ($entity_type->isRevisionable()) {
        $entity_type_id = $entity_type->id();
        $storage = $this->entityTypeManager()->getStorage($entity_type_id);
        /* @var \Drupal\node\NodeStorageInterface $storage */
        $this->origRevisionIds = $storage->revisionIds($entity);
      }
    }
    return $this->origRevisionIds;
  }

  /**
   * Returns the referred entity.
   *
   * @var string|int $version
   *   If method returns current or the latest (possibly unpublished) revision.
   *   Accepted values are 'current', 'latest' or a revision_id as '123'.
   *
   * @return \Drupal\node\NodeInterface|false
   *   Returns the original entity.
   */
  public function originalEntity() {
    if (!isset($this->originalEntity)) {
      /** @var \Drupal\node\NodeInterface[] $orig_entity */
      $orig_entity = $this->get('rid')->referencedEntities();
      $this->originalEntity = reset($orig_entity);
    }
    return $this->originalEntity;
  }

  /**
   * Returns a revision referred entity or FALSE if none exists.
   *
   * @param string|int $revision
   *   If method returns current or the latest (possibly unpublished) revision.
   *   Accepted values are 'current', 'latest', 'origin' or a rid as '123'.
   *
   * @return \Drupal\Core\Entity\EntityInterface|false
   *   Returns a original node revision.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function originalEntityRevision($revision = 'current') {
    $entity = $this->originalEntity();
    $revision_ids = $this->getOrigRevisionIds();

    switch ($revision) {
      case 'current':
        return $entity;

      case 'latest':
        $revision_id = end($revision_ids);
        break;

      case 'origin':
        $revision_id = (int) $this->get('rvid')->getString();
        break;

      default:
        $revision_id = in_array((int) $revision, $revision_ids)
          ? (int) $revision
          : FALSE;
    }

    try {
      if ($revision_id) {
        $entity_type = $entity->getEntityTypeId();
        return $this->entityTypeManager()->getStorage($entity_type)->loadRevision($revision_id);
      }
      else {
        return FALSE;
      }
    }
    catch (\Exception $e) {
      $msgr = \Drupal::messenger();
      $msgr->addError($e->getMessage());
      return FALSE;
    }
  }

  /**
   * Returns a revision referred entity or FALSE if none exists.
   *
   * @return \Drupal\Core\Entity\EntityInterface|false
   *   Returns the original entity in that revision the patch was created from.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function originalEntityRevisionOld() {
    return $this->originalEntityRevision('origin');
  }

  /**
   * Returns the Creator user.
   *
   * @return \Drupal\user\Entity\User|false
   *   Returns the creator user.
   */
  public function getCreator() {
    if (!isset($this->creator)) {
      /** @var \Drupal\user\Entity\User[] $creator */
      $creators = $this->get('uid')->referencedEntities();
      $this->creator = reset($creators) ?: '';
    }
    return $this->creator;
  }

  /**
   * Returns the label belongs to the field type.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup|string
   *   Returns the label belongs to the field type.
   */
  public function getOrigFieldLabel($field_name) {
    if ($orig_entity = $this->originalEntity()) {
      return $orig_entity->getFieldDefinition($field_name)->getLabel();
    }
    else {
      return ucfirst(str_replace('_', ' ', $field_name));
    }
  }

  /**
   * Returns collection of important data to display in head of a patch entity.
   *
   * @param bool $linked_title
   *   If the title should be linked.
   *
   * @return array
   *   Returns data collection for header.
   *
   * @throws EntityMalformedException
   */
  public function getViewHeaderData($linked_title = FALSE) {
    $creator = $this->getCreator();
    $orig_entity = $this->originalEntity();
    $created = $this->get('created')->getValue();
    $created = reset($created);
    try {
      $title = ($linked_title)
        ? $orig_entity->toLink()->toString()
        : $orig_entity->label();
    }
    catch (EntityMalformedException $exception) {
      $msgr = \Drupal::messenger();
      $msgr->addError($exception->getMessage());
    }
    return [
      'created' => ($created) ? $created['value'] : NULL,
      'creator' => ($creator) ? $creator->toLink()->toString() : FALSE,
      'status' => $this->getRenderedStatus(),
      'log_message' => $this->get('message')->getString() ?: FALSE,
      'orig_id' => $this->get('rid')->getString(),
      'orig_type' => ($orig_entity) ? $orig_entity->type->entity->label() : FALSE,
      'orig_title' => (isset($title)) ? $title : FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTagsToInvalidate() {
    $cache_tags = parent::getCacheTagsToInvalidate();
    $entity_type_id = $this->get('rtype')->getString();
    $entity_id = $this->get('rid')->getString();
    $cache_tags[] = "patch_list:{$entity_type_id}:{$entity_id}";
    $cache_tags[] = "{$entity_type_id}:{$entity_id}";
    return $cache_tags;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    $this->invalidateTagsOnSave(TRUE);
    parent::postSave($storage, $update);
  }


}
