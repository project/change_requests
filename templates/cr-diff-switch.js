(function (Drupal, $) {
  'use strict';

  Drupal.behaviors.crDiffSwitch = {

    attach: function(context) {
      $(context).on('click touchend', 'button.cr_btn_switch', function(e) {
        e.preventDefault();
        let $button = $(this);
        let $wrapper = $button.parents('.cr_field_value_wrapper');
        $wrapper.removeClass(['cr-old', 'cr-new', 'cr-cr']);
        switch($button.attr('name')) {
          case 'cr_display_old':
            $wrapper.addClass('cr-old');
            break;
          case 'cr_display_new':
            $wrapper.addClass('cr-new');
            break;
          case 'cr_display_cr':
            $wrapper.addClass('cr-cr');
            break;
          default:
        }
      });
    }
  }

})(Drupal, jQuery);
